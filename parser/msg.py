# -*- coding: utf-8 -*-

from pymystem3 import Mystem
import json
import io


class Message:

    def process(self):
        urls = []
        users = []
        hashtags = []
        for word in self.words:
            if (word[0] == '@'):
                users.append(word)
            elif (word[0] == '#'):
                hashtags.append(word)
            elif (word.find('http') >= 0):
                urls.append(word)

        self.words = [w for w in self.words
                      if w not in urls + users + hashtags]
        self.urls = urls
        self.users = users
        self.hashtags = hashtags

    def show_settings(self):
        print "use urls:\t", self.use_urls
        print "use hashtags:\t", self.use_hashtags
        print "use @users:\t", self.use_users
        print "use 'rt':\t", self.use_retweet

    @staticmethod
    def str2bool(v):
        return v.lower() in ("true")

    def __init__(self, text, configpath):
        self.mystem = Mystem(entire_input=False)
        self.words = [w.strip() for w in filter(None, text.split(' ')) if
                      len(w.strip()) > 0]

        with io.open(configpath, "r") as f:
            settings = json.load(f, encoding='utf8')

        self.use_urls = Message.str2bool(settings['use_urls'])
        self.use_hashtags = Message.str2bool(settings['use_hashtags'])
        self.use_users = Message.str2bool(settings['use_users'])
        self.use_retweet = Message.str2bool(settings['use_retweet'])
        self.garbage_chars = settings['garbage_chars']

        self.process()
