#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import io
import json
from os import listdir, mkdir
from os.path import isfile, join, exists, basename
from parser.msg import Message


def show_progress(message, current):
    print "\rProcessing: %d (%s)" % (current, message),


def smiles_in_words(terms, smiles):
    count = 0
    for w in terms:
        for smile in smiles:
            count += w.count(smile)

    return count


def get_rank(text, filter_config):
    message = Message(text=text, configpath="msg.conf")
    terms = message.words + message.hashtags

    if (len("".join(terms)) < filter_config["message_min_len"]):
        return 0

    pos_smiles = smiles_in_words(terms, filter_config['positive_smiles'])
    neg_smiles = smiles_in_words(terms, filter_config['negative_smiles'])

    pos_threshold = filter_config['positive_threshold']
    neg_threshold = filter_config['negative_threshold']

    if (pos_smiles > pos_threshold and neg_smiles == 0):
        return pos_smiles
    elif (neg_smiles > neg_threshold and pos_smiles == 0):
        return -neg_smiles

    return 0


def parse_line(line):
    args = line.split(';')

    # merge parts of message
    msg = ";".join(args[3:len(args)-6]).replace('\'', '\'\'')
    # remove quotes
    if (len(msg) > 0 and msg[0] == '"' and msg[-1] == '"'):
        msg = msg[1:-1]

    rank = get_rank(msg, filter_config)

    if (rank > 0 or rank < 0):
        line = line.replace('\r\n', '')
        out.writelines(";".join([line, str(rank), '\r\n']))

    return rank

if len(sys.argv) < 2:
    print """Usage: ./filter.py <raw_folder> <output_folder>"""
    exit(0)

# Initialize Configuration Files
with open('filter.conf') as config:
    filter_config = json.load(config, encoding='utf8')

raw_folder = sys.argv[1]
out_folder = sys.argv[2]

if not exists(out_folder):
    mkdir(out_folder)

raw_filepaths = [join(raw_folder, f) for f in listdir(raw_folder)
                 if isfile(join(raw_folder, f))]

for raw_filepath in raw_filepaths:

    raw_filename = basename(raw_filepath)
    out_filepath = join(out_folder, raw_filename)
    twits_processed = 0
    pos_twits = 0
    neg_twits = 0

    print "process: {raw} -> {out}".format(raw=raw_filepath, out=out_filepath)

    with io.open(raw_filepath, 'rt', newline='\r\n') as f:
        with io.open(out_filepath, 'w', newline='\r\n') as out:
            line_index = 0
            for line in f:
                rank = parse_line(line)
                if (rank > 0):
                    pos_twits += 1
                elif (rank < 0):
                    neg_twits += 1

                twits_processed += 1
                show_progress("all: %d| \'+\': %d| \'-\': %d" % (
                              twits_processed, pos_twits, neg_twits),
                              line_index)

                line_index += 1
            print '\n'
